const { defineConfig } = require('cypress')

/** @type {import('cypress').CypressConfig} */
module.exports = defineConfig({
  e2e: {
    baseUrl: 'http://localhost:3000',
  },
  chromeWebSecurity: false,
  browser: 'chrome',
})
