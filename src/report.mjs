import fs from 'fs'
import path, { dirname } from 'path'
import { fileURLToPath } from 'url'

const __dirname = dirname(fileURLToPath(import.meta.url))

/**
 * Sort paths depending on counts
 * @param pathsObj
 * @returns {{[p: string]: unknown}}
 */
function sortPaths(pathsObj) {
  return Object.fromEntries(
    Object.entries(pathsObj)
      // .filter(([, count]) => count > 1)
      .sort((a, b) => b[1] - a[1]),
  )
}

/**
 * Generate CSV report
 * @param data
 * @returns {string}
 */
export function generateCSV(data) {
  const headers = ['Host', 'Method', 'Level', 'Path', 'Count']
  const content = []
  Object.entries(data).forEach(([host, hostObj]) => {
    Object.entries(hostObj).map(([level, lvlObj]) => {
      Object.entries(lvlObj).map(([path, count]) => {
        let [parsedHost, port, method] = host.split(':')
        if (port === '443') {
          parsedHost = `https://${parsedHost}`
        } else if (port === '80') {
          parsedHost = `http://${parsedHost}`
        } else {
          parsedHost = `${parsedHost}:${port}`
        }
        content.push([parsedHost, method, level, path, count].join(','))
      })
    })
  })
  return [headers.join(',')].concat(content).join('\n')
}

export function generateJSON(data) {
  return JSON.stringify(data, null, 2)
}

/**
 * Generate JSON and CSV reports
 * @param paths
 */
export function generateReports(paths) {
  const result = Object.fromEntries(
    Object.entries(paths)
      .map(([host, hostObj]) => [
        host,
        Object.fromEntries(Object.entries(hostObj)
          .map(([level, lvlObj]) => [
            level,
            sortPaths(lvlObj),
          ])),
      ])
      .filter(([, obj]) => !!Object.keys(obj).length))
  const csv = generateCSV(result)
  const json = generateJSON(result)
  fs.writeFileSync(path.join(__dirname, '../report.json'), json)
  fs.writeFileSync(path.join(__dirname, '../report.csv'), csv)
  console.log('[!] JSON and CSV Reports generated')
}
