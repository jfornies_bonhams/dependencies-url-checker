import bodyParser from 'body-parser'
import express from 'express'
import http from 'http'
import https from 'https'
import { emitKeypressEvents } from 'readline'
import { extractHostAndPort, fixUrl, isText, LOCAL_URL, MAIN_HOST_QUERY, MAIN_PORT_QUERY, PORT, replaceUrl } from './common.mjs'
import { generateCSV, generateReports } from './report.mjs'

const app = express()
app.use(bodyParser.urlencoded({ extended: true }))

// Variable to save all paths asked by the browser
let paths = {}

/**
 * Make a request to the url using http or https depending on the destination port
 * @param mainHost
 * @param mainPort
 * @param path
 * @param method
 * @param body
 * @param headers
 * @param originalResponse
 */
function makeRequest({ mainHost, mainPort, path, method, body, headers }, originalResponse) {
  delete headers['host']
  delete headers['accept-encoding']
  // console.log('headers:', headers)
  /** @type {http.RequestOptions} */
  const options = {
    host: mainHost,
    port: mainPort,
    path: decodeURIComponent(path),
    method,
    headers,
    ...(method === 'post' ? { body: body ? JSON.stringify(body) : undefined } : undefined),
  }
  const creq = (mainPort === 443 ? https : http)
    .request(options, (pres) => {
      pres.setEncoding('utf8')

      if ([301, 308].includes(pres.statusCode)) {
        // redirect
        let location = pres.headers.location
        try {
          const newUrl = new URL(location, LOCAL_URL)
          location = newUrl.pathname + newUrl.search
          const [mHost, mPort] = extractMainParams(newUrl, pres.headers)
          mainHost = mHost
          mainPort = mPort
        } catch {
          // do nothing
        }
        getUrl({ url: location, method, headers, body, mainHost, mainPort }, originalResponse)
      } else {
        const validText = isText(pres.headers)
        originalResponse.writeHead(pres.statusCode)
        let data = ''

        pres.on('data', chunk => {
          if (validText) {
            data += chunk
          } else {
            originalResponse.write(chunk)
          }
        })

        pres.on('close', () => {
          if (originalResponse.writableEnded) {
            return
          }
          // closed, let's end client request as well
          if (validText) {
            originalResponse.write(replaceUrl(data))
          }
          originalResponse.end()
        })

        pres.on('end', () => {
          if (originalResponse.writableEnded) {
            return
          }
          // finished, let's finish client request as well
          if (validText) {
            originalResponse.write(replaceUrl(data))
          }
          originalResponse.end()
        })
      }
    })
    .on('error', e => {
      console.error(e.message)
      if (originalResponse.writableEnded) {
        return
      }
      try {
        // attempt to set error message and http status
        originalResponse.writeHead(500)
        originalResponse.write(e.message)
      } catch {
        // ignore
      }
      originalResponse.end()
    })
  creq.end()
}

/**
 * Get an url and return the content changing links to localhost
 * @param path
 * @param method
 * @param headers
 * @param body
 * @param mainHost
 * @param mainPort
 * @param originalResponse
 */
function getUrl({ path, method, headers, body, mainHost, mainPort }, originalResponse) {
  if (!path) {
    path = '/'
  }
  console.log(`${method} ${path} (${mainHost}:${mainPort})`)
  if (!!path?.startsWith('/')) {
    path.split('/').forEach((_, i, arr) => {
      const part = arr.slice(0, i + 1).join('/')
      if (part.startsWith('/')) {
        const lvl = 'level' + i
        const host = `${mainHost}:${mainPort}:${method}`
        if (!paths[host]) {
          paths[host] = {}
        }
        if (!paths[host][lvl]) {
          paths[host][lvl] = {}
        }
        paths[host][lvl][part] = (paths[host][lvl][part] || 0) + 1
      }
    })
  }
  makeRequest({ mainHost, mainPort, path, body, method, headers }, originalResponse)
}

app.get('/____report', (req, res) => {
  const { query } = req
  switch (query?.type) {
    case 'csv':
      res.send(`<code><pre>${generateCSV(paths)}</pre></code>`)
      break
    case 'json':
      res.json(paths)
      break
    case 'generate':
      generateReports(paths)
      res.send('JSON and CSV Reports generated')
      break
    case 'clear':
      paths = {}
      res.send('Data cleared')
      break
    default:
      res.status(400).send('Invalid query {type} = json|csv|generate|clear')
  }
  res.end()
})

function extractMainParams(completeUrl, headers) {
  let mainHost = completeUrl.searchParams.get(MAIN_HOST_QUERY)
  let mainPort = parseInt(completeUrl.searchParams.get(MAIN_PORT_QUERY))
  completeUrl.searchParams.delete(MAIN_HOST_QUERY)
  completeUrl.searchParams.delete(MAIN_PORT_QUERY)
  if (!mainHost || !mainPort) {
    const [host, port] = extractHostAndPort(headers?.referer || headers?.location)
    mainHost = mainHost || host
    mainPort = mainPort || port
  }
  return [mainHost, mainPort]
}

/**
 * Parse request
 * @param req {http.IncomingMessage}
 * @param originalResponse {http.ServerResponse}
 */
function parseRequest(req, originalResponse) {
  const { url, method, headers, body } = req
  const completeUrl = new URL(fixUrl(url), LOCAL_URL)
  let [mainHost, mainPort] = extractMainParams(completeUrl, headers)
  if (!mainHost || !mainPort || mainHost === 'localhost') {
    mainPort = 443
    mainHost = 'www.skinnerinc.com' // Default url
  }
  getUrl({ path: completeUrl.pathname + completeUrl.search, method, headers, body, mainHost, mainPort }, originalResponse)
}

app.get('/*', parseRequest)
app.post('/*', parseRequest)

const server = app.listen(PORT, () => {
  console.log(`[!] Proxy server app listening on PORT ${PORT}`)
  console.log('[+] Press "Ctrl+C" to exit')
  console.log('[+] Press "R" to generate a report')
  console.log('[+] Press "C" to clean data report')
})

process.on('SIGINT', function () {
  console.log('[-] SIGINT received')
  server.close(() => {
    console.log('[!] Proxy server app closed')
    process.exit(0)
  })
})

emitKeypressEvents(process.stdin)

if (process.stdin.isTTY) {
  process.stdin.setRawMode(true)
}

process.stdin.on('keypress', (chunk, key) => {
  // CTRL+C
  if (key?.name === 'c' && key.ctrl) {
    process.emit('SIGINT')
  } else if (key?.name === 'r') {
    generateReports(paths)
  } else if (key?.name === 'c') {
    console.log('[-] Cleaning statistics')
    paths = {}
  }
})
