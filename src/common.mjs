import { ignoreUrls } from './urls.mjs'

export const PORT = +(process.env.PORT || 3000)
export const MAIN_HOST_QUERY = '__mainHost'
export const MAIN_PORT_QUERY = '__mainPort'
export const LOCAL_URL = 'http://localhost:' + PORT

const validTypes = [
  /text\/\w+/i,
  /application\/json/i,
  /application\/javascript/i,
  /application\/xml/i,
  /application\/xhtml\+xml/i,
]

/**
 * Extract the matching urls from the given text
 * @param data
 * @param regexUrl
 * @returns {*|*[]}
 */
export function extractUrl(
  data,
  regexUrl = new RegExp('(?<url>((https?:)?\\/\\/(www\\.)?([-a-zA-Z0-9@:%._+~#=]{2,256}\\.[a-z]{2,6}\\b)+)(:\\d+)?)', 'ig'),
) {
  const result = []
  let match
  while (match = regexUrl.exec(data)) {
    if (match?.groups?.url) {
      result.push(match.groups.url)
    }
  }
  return result.length === 1 ? result[0] : result.filter((x, i, a) => a.indexOf(x) === i)
}

/**
 * Replace the matching urls with the given in query params
 * @param data
 * @returns {*}
 */
export function replaceUrl(data) {
  const regexUrl = new RegExp(
    '(?<url>((https?:)?\\/\\/(?:www\\.)?([-a-zA-Z0-9@:%._\\+~#=]{2,256}\\.[a-z]{2,6}\\b)+)(:\\d+)?([-?/&;@:%._+~#=\\d\\w]*)?)',
    'ig',
  )
  const urlsToReplace = extractUrl(data, regexUrl)
  let newData = data
  const nextToReplace = []
  ;[urlsToReplace].flat().forEach((url) => {
    try {
      let parsedUrl = url
      if (!url.startsWith('http') && url.startsWith('//')) {
        parsedUrl = 'http:' + url
      }
      if (!ignoreUrls.some((ign) => ign.test(url))) {
        const originalUrl = new URL(parsedUrl)
        const localUrl = setHostAndPort(originalUrl, new URL(originalUrl.pathname, LOCAL_URL), originalUrl.searchParams)
        // console.log('Replacing:\t', url.toString(), '\n\t\t', localUrl.toString())
        newData = newData
          .replace(`"${url.toString()}"`, `"${localUrl.toString()}"`)
          .replace(`'${url.toString()}'`, `'${localUrl.toString()}'`)
        nextToReplace.push([url.toString(), localUrl.toString()])
      }
    } catch (e) {
      console.error(e.message)
    }
  })
  nextToReplace.forEach(([url, localUrl]) => {
    newData = newData.replace(url, localUrl)
  })
  return newData
}

/**
 * Add query params for the host and port to the url
 * @param mainUrl
 * @param finalUrl
 * @param queryParams
 * @returns {*}
 */
export function setHostAndPort(mainUrl, finalUrl, queryParams = new URLSearchParams()) {
  const mainPort = mainUrl.port || (mainUrl.protocol.startsWith('https') ? '443' : '80')
  queryParams.forEach((value, key) => {
    finalUrl.searchParams.set(key, value)
  })
  finalUrl.searchParams.set(MAIN_HOST_QUERY, mainUrl.hostname)
  finalUrl.searchParams.set(MAIN_PORT_QUERY, mainPort)
  return finalUrl
}

/**
 * Extract the host and port from url query params
 * @param url
 * @returns {[string, string]}
 */
export function extractHostAndPort(url) {
  try {
    const urlObj = new URL(url)
    const mainHost = urlObj.searchParams.get(MAIN_HOST_QUERY) || urlObj.hostname
    const mainPort = parseInt(urlObj.searchParams.get(MAIN_PORT_QUERY)) || urlObj.port || (urlObj.protocol.startsWith('https') ? '443' : '80')
    return [mainHost, mainPort]
  } catch {
    return ['unknown', 'unknown']
  }
}

/**
 * Fix the url for multiple query params
 * @param url
 * @returns {string|*}
 */
export function fixUrl(url) {
  const params = url.split('?')
  if (params.length > 2) {
    return `${params[0]}?${params.slice(1).join('&')}`
  }
  return url
}

export function transformUrl(url) {
  const mainUrl = new URL(extractUrl(url))
  return setHostAndPort(mainUrl, new URL(replaceUrl(url)))
}

export function isText(headers) {
  return validTypes.some((type) => type.test(headers['content-type']) || type.test(headers['accept']))
}
