// Urls to open in browser
export const urls = [
  'https://www.skinnerinc.com/auctions/past',
  'https://www.skinnerinc.com/auctions/',
  'https://www.skinnerinc.com/auctions/4230M',
  'https://www.skinnerinc.com/auctions/4230M/lots',
  'https://www.skinnerinc.com/auctions/4230M/lots/1',
  'https://www.skinnerinc.com/connect/credits/',
  'https://www.skinnerinc.com/news/blog/',
  'https://www.skinnerinc.com/news/blog/summer-is-for-entertaining/',
  'https://www.skinnerinc.com/news/news/',
  'https://www.skinnerinc.com/news/news/bonhams-skinner-announces-major-americana-auctions-featuring-items-from-the-collections-of-renowned-antiquarians/',
  'https://www.skinnerinc.com/news/category/video/',
  'https://www.skinnerinc.com/news/video/elizabeth-turner-on-lctv-community-conversations/',
  'https://www.skinnerinc.com/news/blog/how-to-look-for-a-paintings-condition/',
  'https://www.skinnerinc.com/news/author/ehaff/',
  'https://www.skinnerinc.com/selling/online-appraisal/',
  // 'https://form.jotform.com/210946378284061',
  // 'https://form.jotform.com/201558172004143',
  // 'https://form.jotform.com/202996581101051',
  // 'https://form.jotform.com/211315665968161',
  // 'https://skinnerinc.us9.list-manage.com/subscribe?u=8e4fdec364a11b8dd4e5abd82&id=349fbe729e',
]

// Urls to ignore in replacement/redirect
export const ignoreUrls = [
  /skinnerinc\.com\/cdn-cgi\/rum/,
  /secure\.skinnerinc\.com/,
  /\.addthis\.com/,
  /\.amazonaws\.com/,
  /googleapis\.com/,
  /google\.com/,
  /youtube\.com/,
  /doubleclick\.com/,
  /w3\.org/,
  /github\.com/,
  /git\.io/,
  /googletagmanager\.com/,
  /cloudflareinsights\.com/,
  /cloudflare\.com/,
  /gravatar\.com/,
  /vimeocdn\.com/,
  /vimeo\.com/,
  /chimpstatic\.com/,
  /mailchimp\.com/,
  /\.jotfor\.ms/,
  /list-manage\.com/,
]
