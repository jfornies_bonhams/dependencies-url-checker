import { exec } from 'child_process'
import { extractUrl, replaceUrl, setHostAndPort, transformUrl } from './common.mjs'
import { urls } from './urls.mjs'

/**
 * Open the url in chrome (macOS)
 * @param url
 */
function openUrl(url) {
  const finalUrl = transformUrl(url)
  console.log('[!] Open url:', finalUrl.toString())
  exec(`open -a "Google Chrome" "${finalUrl.toString()}"`)
}

urls.forEach((url) => {
  openUrl(url)
})
