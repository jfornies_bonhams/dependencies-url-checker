# Dependencies url checker

## Preparation

```bash
yarn install
```

Open `src/urls.mjs` file and edit *urls* variable to add the desired urls to check

## Usage

```bash
yarn server
yarn start
```

Wait for chrome to load all the urls

Type "**R**" and check the results files `report.json` and `report.csv`
