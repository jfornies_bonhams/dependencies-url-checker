import expect from 'node:assert/strict'
import { describe, it } from 'node:test'
import { extractUrl, replaceUrl, transformUrl } from '../src/common.mjs'

describe.skip('extractUrl', () => {
  it('should extract https url', () => {
    const result = extractUrl('https://www.sample.com/path/more')
    expect.equal(result, 'https://www.sample.com')
  })
  it('should extract without www', () => {
    const result = extractUrl('http://sample.com/path/more')
    expect.equal(result, 'http://sample.com')
  })
  it('should extract without http', () => {
    const result = extractUrl('//sample.com/path/more')
    expect.equal(result, '//sample.com')
  })
  it('should extract with port', () => {
    const result = extractUrl('//sample.com:4000/path/more')
    expect.equal(result, '//sample.com:4000')
  })
  it('should extract multiple urls', () => {
    const result = extractUrl(`https://www.sample.com/path/more
http://sample.com/path/more
//sample.com/path/more`)
    expect.deepEqual(result, ['https://www.sample.com', 'http://sample.com', '//sample.com'])
  })
})

describe.skip('replaceUrl', () => {
  it('should replace https url', () => {
    const result = replaceUrl('https://www.sample.com/path/more')
    expect.equal(result, 'http://localhost:3000/path/more?__mainHost=www.sample.com&__mainPort=443')
  })
  it('should replace without protocol', { only: true }, () => {
    const result = replaceUrl('//www.sample.com/path/more')
    expect.equal(result, 'http://localhost:3000/path/more?__mainHost=www.sample.com&__mainPort=80')
  })
  it('should replace multiple urls', () => {
    const result = replaceUrl(`https://www.sample.com/path1/more
http://sample.com/path2/more
https://sample.com:5000/path500/more
//sample.com/path3/more`)
    expect.equal(result, `http://localhost:3000/path1/more?__mainHost=www.sample.com&__mainPort=443
http://localhost:3000/path2/more?__mainHost=sample.com&__mainPort=80
http://localhost:3000/path500/more?__mainHost=sample.com&__mainPort=5000
http://localhost:3000/path3/more?__mainHost=sample.com&__mainPort=80`)
  })
})

describe('transformUrl', () => {
  it('should transform to correct url', () => {
    const result = transformUrl('https://www.sample.com/path/more')
    expect.equal(result.toString(), 'http://localhost:3000/path/more?__mainHost=www.sample.com&__mainPort=443')
  })
})
