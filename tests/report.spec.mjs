import fs from 'fs'
import expect from 'node:assert/strict'
import { describe, it } from 'node:test'
import path, { dirname } from 'path'
import { fileURLToPath } from 'url'
import { generateCSV } from '../src/report.mjs'
import reportJson from './mock/report.mock.json' assert { type: 'json' }

const __dirname = dirname(fileURLToPath(import.meta.url))

describe('generateCsv', () => {
  it('should return the correct data', () => {
    const result = generateCSV(reportJson) + '\n'
    expect.equal(fs.readFileSync(path.join(__dirname, './mock/report.mock.csv'), 'utf-8'), result)
  })
})
