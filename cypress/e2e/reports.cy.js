/// <reference types="cypress" />

const { urls } = require('../../src/urls.mjs')
const { transformUrl } = require('../../src/common.mjs')

context('Reports', () => {
  context('Clear reports data', () => {
    it('clean reports', () => {
      cy.visit('http://localhost:3000/____report?type=clear', { timeout: 60000 })
      cy.contains('Data cleared')
    })
  })
  context('Run all urls', () => {
    const regularWait = 2_000 // 2 seconds
    const formUrlMap = new Map()
    urls.forEach((url) => {
      const finalUrl = transformUrl(url)
      it(`load and scroll for ${url} => ${finalUrl.toString()}`, () => {
        cy.on('uncaught:exception', (err, runnable) => {
          console.log('[!] Uncaught exception', err)
          return false
        })
        console.log('[!] Open url:', finalUrl.toString())
        cy.visit(finalUrl.toString(), { timeout: 60000 })
        cy.wait(regularWait)
        cy.scrollTo('center')
        cy.wait(regularWait)
        cy.scrollTo('bottom')
        cy.wait(regularWait)
        cy.url().should('include', finalUrl)
        cy.document().then((doc) => {
          const forms = [...doc.forms]
          cy.log(`[!] Number of forms: ${forms.length}`)
          forms.forEach((form, n) => {
            const action = form.action
            if (!formUrlMap.has(action) && typeof form?.submit === 'function') {
              formUrlMap.set(action, true)
              cy.log(`[!] Submitting form ${n}`)
              form.submit()
              cy.wait(regularWait)
              cy.visit(finalUrl.toString(), { timeout: 60000 })
            }
          })
        })
      })
    })
  })
  context('Generate reports', () => {
    it('generate json and csv reports', () => {
      cy.visit('http://localhost:3000/____report?type=generate', { timeout: 60000 })
      cy.contains('JSON and CSV Reports generated')
    })
  })
})
